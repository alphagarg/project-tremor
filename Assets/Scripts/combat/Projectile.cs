﻿using UnityEngine;

public class Projectile : MonoBehaviour
{
	public GameObject effect;
	public Vector3 translate;
	public Vector3 rotation;
	public float speed;
	public float damage;
	public float range;
	public float time;
	public Rigidbody rbody;
	public float bounceChance;
	
	public void SetUp(Vector3 trnslt, Vector3 rttn, float spd, float dmg, float rng, float tm, bool phscs, float bounce)
	{
		translate = trnslt;
		rotation = rttn;
		speed = spd;
		damage = dmg;
		range = rng;
		time = tm;
		bounceChance = bounce;
		
		if (phscs)
		{
			rbody = gameObject.AddComponent<Rigidbody>();
			rbody.mass = dmg / 10;
			GetComponent<Collider>().isTrigger = false;
		}
	}

	void FixedUpdate()
	{
		time -= Time.fixedDeltaTime;
		Vector3 rayDir = transform.forward;
		if (rbody)
			rayDir = rbody.velocity;
		if (Physics.Raycast(transform.position, rayDir, out GameMan.hit, speed * Time.fixedDeltaTime)) //Go through portals
		{
			if (GameMan.hit.transform.parent)
			{
				Portal p = GameMan.hit.transform.parent.GetComponent<Portal>();
				if (p)
				{
					transform.position = GameMan.hit.point + transform.forward * 0.1f;
					
					Transform portalIn = GameMan.hit.transform;
					Transform portalOut;
					if (p.portalOne == GameMan.hit.transform)
						portalOut = p.portalTwo;
					else
						portalOut = p.portalOne;
					
					p.TeleportObject(GetComponent<Collider>(), portalIn, portalOut);
					
					return;
				}
			}
		}
		if (!rbody && Physics.Raycast(transform.position, transform.forward, out GameMan.hit, speed * Time.fixedDeltaTime))
		{
			if (Random.Range(0.0f, 1.0f) < bounceChance && GameMan.hit.collider.tag != "Damageable" && Vector3.Angle(GameMan.hit.normal, transform.forward) < 60)
			{
				transform.LookAt(transform.position + transform.forward * Random.Range(0f, 0.8f) + Vector3.Reflect(transform.forward, GameMan.hit.normal));
				time /= 2;
				speed /= 2;
			}
			else
			{
				time = 0;
				HandleHit(GameMan.hit.collider, GameMan.hit.point, GameMan.hit.normal);
			}
		}
		
		if (time <= 0)
		{
			if (range > 0)
			{
				Collider[] c = Physics.OverlapSphere(transform.position, range / 2);
				for (int i = 0; i < c.Length; i++)
				{
					HandleHit(c[i], transform.position, -transform.forward);
				}
			}
			
			Destroy(Instantiate(effect, transform.position, effect.transform.rotation), 5);
			Destroy(gameObject);
		}
		
		transform.position += (transform.right * translate.x + transform.up * translate.y + transform.forward * translate.z) * speed * Time.fixedDeltaTime;
		transform.eulerAngles += rotation * Time.fixedDeltaTime * 100;
	}
	
	//This only gets called if it's a rigidbody, so no need to null check rbody
	void OnCollisionEnter(Collision col)
	{
		HandleHit(col.collider, col.GetContact(0).point, col.GetContact(0).normal);
	}
	
	void HandleHit(Collider col, Vector3 point, Vector3 normal)
	{
		if (col.tag == "Damageable")
		{
			Player p = col.GetComponent<Player>();
			if (p)
			{
				p.ReceiveDamage(damage);
				Vector3 boost = (normal + (p.transform.position - point).normalized / 2) * range / 40;
				p.displacementHorizontal += boost - Vector3.up * boost.y;
				p.displacementVertical += Vector3.up * boost.y;
				p.airtime += range / 80;
			}
			else
				Destroy(col.gameObject);
			
			time = 0;
		}
	}
}
