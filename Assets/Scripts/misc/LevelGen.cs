﻿using UnityEngine;
using UnityEngine.AI;

public enum LevelPartsType
{
	Blockout,
	Gothic,
	Medieval
}

public class LevelGen : MonoBehaviour
{
	public int length = 50;
	public LevelPartsType LPT;
	public GameObject blockoutFrame;
	public GameObject[] blockout;
	public GameObject[] gothic;
	public GameObject[] medieval;
	public GameObject portals;
	public NavMeshSurface NoMansSky;
	
	void Start()
	{
		GameObject[] parts;
		GameObject frame;
		switch (LPT)
		{
			case LevelPartsType.Gothic:
				parts = gothic;
				frame = blockoutFrame;
				break;
			case LevelPartsType.Medieval:
				parts = medieval;
				frame = blockoutFrame;
				break;
			default:
				parts = blockout;
				frame = blockoutFrame;
				break;
		}
		
		Transform exit = null;
		for (int i = 0; i < length; i++)
		{
			Transform part = Instantiate(parts[Random.Range(0, parts.Length)]).transform;
			Transform entryway = part.GetChild(Random.Range(2, part.childCount));
			entryway.parent = transform;
			part.parent = entryway;
			
			if (exit)
			{
				entryway.position = exit.position;
				entryway.localScale = exit.localScale;
				entryway.eulerAngles = exit.eulerAngles + Vector3.up * 180;
				
				Bounds b = part.Find("wall").GetComponent<MeshCollider>().bounds;
				MeshCollider exitMC = exit.GetComponentInChildren<MeshCollider>();
				bool notIntersectingAnything = true;
				MeshCollider[] MCs = GetComponentsInChildren<MeshCollider>();
				for (int o = 0; o < MCs.Length && notIntersectingAnything; o++)
				{
					if (MCs[o] && MCs[o].bounds != b && MCs[o] != exitMC && b.Intersects(MCs[o].bounds))
						notIntersectingAnything = false;
				}
				
				if (!notIntersectingAnything)
				{
					Portal prtl = Instantiate(portals).GetComponent<Portal>();
					prtl.portalOne.position = exit.position;
					prtl.portalOne.rotation = exit.rotation;
					//prtl.portalOne.localScale = Vector3.Lerp(exit.localScale, entryway.localScale, 0.5f);
					prtl.portalOne.localScale = exit.localScale;
					entryway.position += new Vector3(Random.Range(-50, 50), Random.Range(10, 40), Random.Range(-50, 50));
					prtl.portalTwo.position = entryway.position;
					prtl.portalTwo.rotation = entryway.rotation;
					prtl.portalTwo.localScale = prtl.portalOne.localScale;
					prtl.portalTwo.localEulerAngles += Vector3.up * 180;
				}
				
				Transform hole = entryway;
				Transform frm = Instantiate(frame, hole.position, hole.rotation).transform;
				frm.eulerAngles += Vector3.up * 180;
				if (notIntersectingAnything)
					Destroy(frm.GetChild(1).gameObject);
				else
					frm.GetChild(1).GetComponent<Collider>().enabled = false;
				
				while (part.Find("ENTRYWAY"))
				{
					hole = part.Find("ENTRYWAY");
					frm = Instantiate(frame, hole.position, hole.rotation).transform;
					frm.eulerAngles += Vector3.up * 180;
					hole.name = "EXIT";
				}
			}
			else
			{
				entryway.position = Vector3.forward * -2 - Vector3.up;
				entryway.rotation = Quaternion.LookRotation(-Vector3.forward);
				entryway.localScale = new Vector3(Random.Range(0.75f, 1.25f), Random.Range(0.875f, 1.125f), Random.Range(0.75f, 1.25f)) * 1.5f;
				
				Transform hole = entryway;
				Transform frm = Instantiate(frame, hole.position, hole.rotation).transform;
				frm.eulerAngles += Vector3.up * 180;
				while (part.Find("ENTRYWAY"))
				{
					hole = part.Find("ENTRYWAY");
					frm = Instantiate(frame, hole.position, hole.rotation).transform;
					frm.eulerAngles += Vector3.up * 180;
					hole.name = "EXIT";
				}
			}
			
			exit = part.GetChild(Random.Range(2, part.childCount));
		}
		
		NoMansSky.BuildNavMesh();
	}
}
