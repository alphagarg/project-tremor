﻿using UnityEngine;
using System;
using System.Collections.Generic;

public class PortalTrackable : MonoBehaviour
{
	public Vector3 previousOffsetFromPortal;
}
public class Portal : MonoBehaviour
{
	Camera mainCam;
	public Transform portalOne;
	public Transform portalTwo;
	MeshRenderer POMR;
	MeshRenderer PTMR;
	Camera one;
	Camera two;
	void Start()
	{
		one = new GameObject(portalOne.name + "Cam").AddComponent<Camera>();
		two = new GameObject(portalTwo.name + "Cam").AddComponent<Camera>();
		
		one.enabled = false;
		two.enabled = false;
		
		POMR = portalOne.GetComponent<MeshRenderer>();
		RenderTexture rt = new RenderTexture(Screen.width, Screen.height, 0, UnityEngine.Experimental.Rendering.DefaultFormat.LDR);
		POMR.material = Instantiate(POMR.material);
		POMR.material.SetTexture("_MainTex", rt);
		one.targetTexture = rt;
		
		PTMR = portalTwo.GetComponent<MeshRenderer>();
		rt = new RenderTexture(Screen.width, Screen.height, 0, UnityEngine.Experimental.Rendering.DefaultFormat.LDR);
		PTMR.material = Instantiate(PTMR.material);
		PTMR.material.SetTexture("_MainTex", rt);
		two.targetTexture = rt;
		
		POTs = new List<PortalTrackable>();
		PTTs = new List<PortalTrackable>();
		POCols = new Collider[0];
		PTCols = new Collider[0];
	}
	void LateUpdate()
	{
		if (!mainCam && GameObject.FindObjectOfType<Player>())
			mainCam = GameObject.FindObjectOfType<Player>().camComponent;
		
		if (!mainCam)
			return;
		
		if (VisibleFromCamera(mainCam, POMR) || VisibleFromCamera(mainCam, PTMR))
		{
			one.fieldOfView = 88;
			two.fieldOfView = 88;
			
			POMR.enabled = false;
			PTMR.enabled = false;
			
			var matrix = portalTwo.localToWorldMatrix * portalOne.worldToLocalMatrix * mainCam.transform.localToWorldMatrix;
			one.transform.SetPositionAndRotation(matrix.GetColumn(3), matrix.rotation);
			
			matrix = portalOne.localToWorldMatrix * portalTwo.worldToLocalMatrix * mainCam.transform.localToWorldMatrix;
			two.transform.SetPositionAndRotation(matrix.GetColumn(3), matrix.rotation);
			
			SetNearClipPlane(one, portalTwo);
			SetNearClipPlane(two, portalOne);
			
			one.Render();
			two.Render();
			
			POMR.enabled = true;
			PTMR.enabled = true;
		}
	}
	float portalThickness = 0;
	void FixedUpdate()
	{
		if (!mainCam || ((mainCam.transform.position - portalOne.position).sqrMagnitude > 16 * 16 && (mainCam.transform.position - portalTwo.position).sqrMagnitude > 16 * 16))
			return;
		
		if (portalThickness == 0)
			portalThickness = (2 + 1 / Time.fixedDeltaTime) * 0.34f;
		
		portalOne.localScale += Vector3.forward * portalThickness;
		portalTwo.localScale += Vector3.forward * portalThickness;
		POCols = TeleportCollidersInVolume(POMR, portalOne, portalTwo, POCols, POTs);
		PTCols = TeleportCollidersInVolume(PTMR, portalTwo, portalOne, PTCols, PTTs);
		portalOne.localScale -= Vector3.forward * portalThickness;
		portalTwo.localScale -= Vector3.forward * portalThickness;
	}
	List<PortalTrackable> POTs;
	List<PortalTrackable> PTTs;
	Collider[] POCols;
	Collider[] PTCols;
	Collider[] TeleportCollidersInVolume(MeshRenderer MR, Transform portalIn, Transform portalOut, Collider[] lastPCols, List<PortalTrackable> PTs)
	{
		Collider[] PCols = Physics.OverlapBox(MR.bounds.center, MR.bounds.extents, portalIn.rotation);
		bool found = false;
		for (int o = 0; o < lastPCols.Length; o++)
		{
			PortalTrackable pt;
			for (int i = 0; i < PCols.Length; i++)
			{
				//Track the object
				pt = PCols[i].GetComponent<PortalTrackable>();
				if (!pt && (PCols[i].GetComponent<Rigidbody>() || PCols[i].GetComponent<CharacterController>()))
					pt = PCols[i].gameObject.AddComponent<PortalTrackable>();
				
				if (pt)
				{
					//Add the object to the list if it's not in it yet
					if (!PTs.Contains(pt))
					{
						pt.previousOffsetFromPortal = pt.transform.position - portalIn.position;
						PTs.Add(pt);
					}
					
					//Check if it's crossed sides
					Vector3 offsetFromPortal = pt.transform.position - portalIn.position;
					if (offsetFromPortal != pt.previousOffsetFromPortal)
					{
						int portalSide = Math.Sign(Vector3.Dot(offsetFromPortal, portalIn.forward));
						int portalSideOld = Math.Sign(Vector3.Dot(pt.previousOffsetFromPortal, portalIn.forward));
						//Teleport them
						if (portalSide != portalSideOld)
						{
							//portalOut.localEulerAngles += Vector3.up * 180;
							TeleportObject(PCols[i], portalIn, portalOut);
							PTs.Remove(pt);
							Destroy(pt);
							//portalOut.localEulerAngles -= Vector3.up * 180;
						}
					}
				}
				//For cleaning PTs that are no longer being used, and freeing them up for other portals. If it's found, then it stays in the list.
				if (lastPCols[o] == PCols[i])
					found = true;
			}
			if (!found && lastPCols[o])
			{
				pt = lastPCols[o].GetComponent<PortalTrackable>();
				PTs.Remove(pt);
				Destroy(pt);
			}
		}
		return PCols;
	}
	public void TeleportObject(Collider PCol, Transform portalIn, Transform portalOut)
	{
		PCol.transform.position = portalOut.TransformPoint(portalIn.InverseTransformPoint(PCol.transform.position));
		PCol.transform.rotation = Quaternion.LookRotation(portalOut.TransformDirection(portalIn.InverseTransformDirection(PCol.transform.forward)));
		
		if (PCol.attachedRigidbody)
			PCol.attachedRigidbody.velocity = portalOut.TransformDirection(portalIn.InverseTransformDirection(PCol.attachedRigidbody.velocity));
		else
		{
			Player p = PCol.GetComponent<Player>();
			if (p)
			{
				p.displacementHorizontal = portalOut.TransformDirection(portalIn.InverseTransformDirection(p.displacementHorizontal));
				p.displacementVertical = portalOut.TransformDirection(portalIn.InverseTransformDirection(p.displacementVertical));
			}
		}
	}
	
	bool VisibleFromCamera(Camera c, Renderer r)
	{
		if ((c.transform.position - r.bounds.center).sqrMagnitude > 25 * 25)
			return false;
		
		Plane[] frustumPlanes = GeometryUtility.CalculateFrustumPlanes(c);
		return GeometryUtility.TestPlanesAABB(frustumPlanes, r.bounds);
	}
	void SetNearClipPlane(Camera c, Transform clipPlane)
	{
		int dot = Math.Sign(Vector3.Dot(clipPlane.forward, clipPlane.position + c.transform.forward * 0.02f - c.transform.position));
		
		Vector3 camSpacePos = c.worldToCameraMatrix.MultiplyPoint(clipPlane.position + c.transform.forward * 0.02f);
		Vector3 camSpaceNormal = c.worldToCameraMatrix.MultiplyVector(clipPlane.forward) * dot;
		float camSpaceDist = -Vector3.Dot(camSpacePos, camSpaceNormal);
		Vector4 clipPlaneCameraSpace = new Vector4(camSpaceNormal.x, camSpaceNormal.y, camSpaceNormal.z, camSpaceDist);
		
		c.projectionMatrix = c.CalculateObliqueMatrix(clipPlaneCameraSpace);
	}
}
