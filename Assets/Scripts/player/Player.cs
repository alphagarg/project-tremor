﻿using UnityEngine;

[System.Serializable]
public struct Inventory
{
	public AudioClip[] attack;
	public AudioClip[] altAttack;
	public GameObject weapon;
	public GameObject projectileOrImpactEffect;
	public Transform source;
	public weaponType type;
	public float cooldown;
	public float damage;
	public float spread;
	public bool pickedUp;
}

public enum weaponType
{
	projectile,
	hitscan,
	melee
}

public class Player : MonoBehaviour
{
	//Movement components
	CharacterController cc;
	Transform t;
	
	//Movement parameters
	public float speedScale = 16;
	public float accelerationScale = 8;
	public float jumpScale = 4;
	public bool jumped;
	
	//Movement attributes
	Vector3 lastPosition;
	public Vector3 displacementHorizontal;
	public Vector3 displacementVertical;
	public float airtime = 1;
	
	//..//..//..//..//..//..//..//..//..//
	
	//Mouselook components
	public Transform cam;
	public Camera camComponent;
	
	//Mouselook parameters
	public float mouselookSpeed = 1;
	public float clampX = 80;
	public float cameraZAngle = 0;
	public bool lockCursor = true;
	
	//..//..//..//..//..//..//..//..//..//
	
	//Combat components
	public Animation gun;
	
	//Combat parameters
	public float health = 100;
	public Transform weaponRoot;
	public Inventory[] weapons;
	public int equipped;
	
	//Combat attributes
	float nextShot;

	public void ReceiveDamage(float dam = 0)
	{
		health -= dam;
	}
	
	void Start()
	{
		cc = GetComponent<CharacterController>();
		t = transform;
		camComponent = cam.GetComponent<Camera>();
		camComponent.fieldOfView = 360;
	}

	//TODO Add acceleration, implement velocity, make air control acceleration half
	void Update()
	{
		Input_Horizontal();
		Input_Vertical();
		
		Mouselook();
		
		EquipmentInput();
		Combat();
	}
	
	void FixedUpdate()
	{
		Movement_Horizontal();
		Movement_Vertical();
		
		Movement_Displace();
	}
	
	#region Movement
	void Input_Horizontal()
	{
		v = Input.GetAxis("Vertical") * airtime;
		hPure = Input.GetAxis("Horizontal");
		h = Mathf.Lerp(hPure * airtime, 0, v * 0.25f - 0.25f);
	}
	void Input_Vertical()
	{
		if (Input.GetButtonUp("Jump"))
			jumped = false;
	}
	float v;
	float hPure;
	float h;
	void Movement_Horizontal()
	{
		Vector3 _disp = Vector3.zero;
		
		_disp = v * t.forward * airtime + h * t.right / (0.5f + airtime * (v * v) * 0.5f);
		_disp *= speedScale;
		
		float moveLerp = Time.fixedDeltaTime * (accelerationScale - (displacementHorizontal).sqrMagnitude * Time.fixedDeltaTime);
		if (cc.isGrounded)
		{
			displacementHorizontal = Vector3.Lerp(displacementHorizontal, _disp, moveLerp);
			
			airtime = Mathf.Lerp(airtime, 1, Time.fixedDeltaTime * 4);
		}
		else
		{
			displacementHorizontal = Vector3.Lerp(displacementHorizontal, _disp, moveLerp * (h * h + v * v) * 0.5f);
			
			float plusH = hPure;
			if (plusH < 0)
				plusH = -plusH;
			airtime = Mathf.Lerp(airtime, 4 + plusH, Time.fixedDeltaTime * plusH / 20);
		}
		
		if (cc.isGrounded)
			displacementHorizontal = Vector3.Lerp(displacementHorizontal, Vector3.zero, Time.fixedDeltaTime * 10);
		
		lastPosition = t.position;
	}
	void Movement_Vertical()
	{
		if (cc.isGrounded)
		{
			if (!jumped && Input.GetButton("Jump"))
			{
				displacementVertical = Vector3.up * jumpScale;
				jumped = true;
			}
			else if (displacementVertical.y < 0)
				displacementVertical = Vector3.zero;
		}
		else if (displacementVertical.y > -9.81f * Time.fixedDeltaTime)
			displacementVertical -= Vector3.up * 9.81f * Time.fixedDeltaTime * 0.025f;
	}
	void Movement_Displace()
	{
		cc.Move(displacementHorizontal * Time.deltaTime + displacementVertical);
	}
	void OnControllerColliderHit(ControllerColliderHit hit)
	{
		float normSqr = (hit.normal - Vector3.up).sqrMagnitude;
		if (normSqr > 0.5f)
			displacementHorizontal += hit.normal * airtime;
		else
			displacementVertical += Vector3.up * normSqr * airtime * displacementHorizontal.sqrMagnitude * Time.fixedDeltaTime * 0.2f;
	}
	#endregion
	
	void Mouselook()
	{
		cam.position = t.position + Vector3.up * 2;
		camComponent.fieldOfView = Mathf.Lerp(camComponent.fieldOfView, 90, Time.deltaTime * 4);
		
		if (lockCursor)
			Cursor.lockState = CursorLockMode.Locked;
		else
			Cursor.lockState = CursorLockMode.None;
		
		Vector2 ogAng = new Vector2(cam.eulerAngles.x, cam.eulerAngles.y);
		Vector2 mlAng = new Vector2(-Input.GetAxis("Mouse Y"), Input.GetAxis("Mouse X"));
		
		mlAng *= mouselookSpeed;
		
		Vector2 finalAng = ogAng + mlAng;
		
		if (finalAng.x > 270)
			finalAng.x -= 360;
		
		if (finalAng.x > clampX && ogAng.x - 5 < clampX)
			finalAng.x = clampX;
		if (finalAng.x < -clampX && ogAng.x - 5 > -clampX)
			finalAng.x = -clampX;
		
		t.eulerAngles = new Vector3(0, finalAng.y, t.eulerAngles.z);
		cam.eulerAngles = new Vector3(finalAng.x, finalAng.y, cameraZAngle);
		
		cameraZAngle -= cameraZAngle * Time.deltaTime * 10;
		
		weaponRoot.eulerAngles += (Vector3.right * displacementVertical.y * 8 + Vector3.right * mlAng.x / 8) + (Vector3.up * mlAng.y / 8);
		cameraZAngle += mlAng.y / 8 + Input.GetAxis("Horizontal") * Time.deltaTime * displacementHorizontal.sqrMagnitude / 8;
	}
	
	#region Combat
	void EquipmentInput()
	{
		if (Input.GetButtonDown("Next Weapon") || Input.GetAxis("Mouse Scrollwheel") > 0.1f)
		{
			if (equipped + 1 == weapons.Length)
			{
				if (weapons[0].pickedUp)
					EquipWeapon(0);
			}
			else
			{
				if (weapons[equipped + 1].pickedUp)
					EquipWeapon(equipped + 1);
			}
		}
		else if (Input.GetButtonDown("Previous Weapon") || Input.GetAxis("Mouse Scrollwheel") < -0.1f)
		{
			if (equipped == 0)
			{
				if (weapons[weapons.Length - 1].pickedUp)
					EquipWeapon(weapons.Length - 1);
			}
			else
			{
				if (weapons[equipped - 1].pickedUp)
					EquipWeapon(equipped - 1);
			}
		}
		else if (Input.GetButtonDown("Bucket 1"))
		{
			EquipWeapon(0);
		}
		else if (Input.GetButtonDown("Bucket 2"))
		{
			EquipWeapon(1);
		}
	}
	void Combat()
	{
		weaponRoot.rotation = Quaternion.Lerp(weaponRoot.rotation, Quaternion.Euler(cam.eulerAngles.x + 7.5f, cam.eulerAngles.y, cameraZAngle * 3), Time.deltaTime * (5 + Mathf.Clamp(((nextShot - Time.time) / weapons[equipped].cooldown) * 5, 0, 4)));
		
		if (Input.GetButton("Fire") && nextShot <= Time.time)
		{
			Inventory w = weapons[equipped];
			
			nextShot = Time.time + w.cooldown;
			w.source.GetComponent<ParticleSystem>().Play();
			weaponRoot.eulerAngles -= Vector3.right * 15 * Mathf.Lerp(w.cooldown, 1, 0.25f);
			camComponent.fieldOfView += 2 + 2 * w.cooldown;
			
			if (w.type == weaponType.hitscan)
			{
				for (int i = 0; i < 36; i++)
				{
					RaycastHit hit;
					if (Physics.Raycast(cam.position, cam.forward + new Vector3(Random.Range(-1f, 1f), Random.Range(-1f, 1f), Random.Range(-1f, 1f)) / 5, out hit, 50, ~(1 << 8)))
					{
						Destroy(Instantiate(w.projectileOrImpactEffect, hit.point, Quaternion.LookRotation(hit.normal - cam.forward)), 2);
					}
				}
			}
			else if (w.type == weaponType.projectile)
			{
				Instantiate(w.projectileOrImpactEffect, w.source.position, cam.rotation).GetComponent<Projectile>().SetUp(Vector3.forward + Vector3.up / 10, Vector3.forward * 5 + Vector3.right * 0.02f, 30, 120, 5, 5, false, 0.1f);
			}
			else if (w.type == weaponType.melee)
			{
				
			}
		}
		else if (Input.GetButton("Alt-Fire"))
		{
			
		}
	}
	
	
	
	void EquipWeapon(int index)
	{
		weaponRoot.eulerAngles = cam.eulerAngles + Vector3.right * 60 - Vector3.forward * 60;
		weapons[equipped].weapon.SetActive(false);
		weapons[index].weapon.SetActive(true);
		equipped = index;
	}
	#endregion
}
