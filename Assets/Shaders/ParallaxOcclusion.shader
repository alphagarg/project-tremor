﻿Shader "Custom/ParallaxOcclusion" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		[Normal] [NoScaleOffset] _BumpMap ("Normal map (RGB)", 2D) = "bump" {}
		_BumpScale ("Bump scale", Range(0,1)) = 1
		[NoScaleOffset] _ParallaxMap ("Height map (R)", 2D) = "white" {}
		_Parallax ("Height scale", Range(0,1)) = 0.05
		_RefPlane ("Reference Plane", Float) = 1
		[NoScaleOffset] _Metallic ("Metallic", 2D) = "black" {}
		_Glossiness ("Smoothness", Range(0,1)) = 0.5
        [Enum(Metallic Alpha,0,Albedo Alpha,1,Normal Alpha,2)] _SmoothnessChannel ("Smoothness Texture Channel", Float) = 0
		[IntRange] _ParallaxMinSamples ("Parallax min samples", Range(2,100)) = 4
		[IntRange] _ParallaxMaxSamples ("Parallax max samples", Range(2,100)) = 20
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM
		#pragma surface surf Standard fullforwardshadows vertex:vert

		#pragma target 3.0
		
		sampler2D _MainTex;
		sampler2D _BumpMap;
		sampler2D _ParallaxMap;
		float4 _MainTex_ST;
		
		struct Input {
			float2 texcoord;
			float3 eye;
			float sampleRatio;
		};

		half _Glossiness;
		sampler2D _Metallic;
		half _BumpScale;
		half _Parallax;
		float _RefPlane;
		fixed4 _Color;
		uint _ParallaxMinSamples;
		uint _ParallaxMaxSamples;
		uint _SmoothnessChannel;
		
		#include<ParallaxOcclusion.cginc>
		
		void vert(inout appdata_full IN, out Input OUT) {
			parallax_vert( IN.vertex, IN.normal, IN.tangent, OUT.eye, OUT.sampleRatio );
			OUT.texcoord = TRANSFORM_TEX(IN.texcoord, _MainTex);
		}

		void surf (Input IN, inout SurfaceOutputStandard o) {
			float2 offOffset = ((-IN.eye.xy) / IN.eye.z) * _Parallax /2 * ((1 - _RefPlane) * (-1));
			float2 offset = parallax_offset (_Parallax, IN.eye, IN.sampleRatio, IN.texcoord + offOffset, 
			_ParallaxMap, _ParallaxMinSamples, _ParallaxMaxSamples) + offOffset;
			float2 uv = IN.texcoord + offset;
			fixed4 c = tex2D (_MainTex, uv) * _Color;
			fixed4 m = tex2D (_Metallic, uv);
			fixed4 n = tex2D (_BumpMap, uv);
			o.Albedo = c.rgb;
			o.Normal = UnpackScaleNormal(n, _BumpScale);
			o.Metallic = m.rgb;
			o.Alpha = c.a;
			
			if(_SmoothnessChannel == 2){
				o.Smoothness = _Glossiness * n.a;
			}
			else if(_SmoothnessChannel == 1){
				o.Smoothness = _Glossiness * c.a;
			}
			else{
				o.Smoothness = _Glossiness * m.a;
			}
		}
		
		ENDCG
	}
	FallBack "Diffuse"
}